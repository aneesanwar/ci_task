-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 03, 2019 at 11:20 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `uname` varchar(500) NOT NULL,
  `pass` varchar(500) NOT NULL,
  `on_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `uname`, `pass`, `on_create`, `login_status`) VALUES
(1, 'Raheel', 'raheel@gmail.com', 'raheel.shehzad', '21232f297a57a5a743894a0e4a801fc3', '2019-03-02 14:41:09', 0),
(2, 'anees', 'anees@gm.com', 'anees.ims@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '2019-03-03 14:35:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

DROP TABLE IF EXISTS `user_log`;
CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1= login, 2=logout',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `user_id`, `type`, `time`) VALUES
(1, 1, 1, '2019-03-03 15:44:20'),
(2, 1, 2, '2019-03-03 15:47:50'),
(3, 1, 1, '2019-03-03 15:52:33'),
(4, 1, 2, '2019-03-03 15:52:38'),
(5, 1, 2, '2019-03-03 15:57:35'),
(6, 1, 1, '2019-03-03 15:58:05'),
(7, 1, 2, '2019-03-03 15:58:53'),
(8, 1, 1, '2019-03-03 15:59:51'),
(9, 1, 2, '2019-03-03 16:00:26'),
(10, 1, 1, '2019-03-03 16:00:45'),
(11, 1, 1, '2019-03-03 16:09:08'),
(12, 1, 1, '2019-03-03 16:11:14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
