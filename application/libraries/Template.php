<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
	var $ci;
	function __construct() {
		$this->ci =& get_instance();
	}

    function load_auth_page($view , $data = array())
    {
    	$this->ci->load->view('template/auth/head.php' , $data);
    	$this->ci->load->view('template/auth/'.$view.'.php' , $data);
    	$this->ci->load->view('template/auth/footer.php' , $data);
    }

}
	
?>