<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$check_login = $this->check_login();
		if($check_login)
		{
			echo "Welcome ". $_SESSION['login']['name'] ."! <br><br>";
			echo '<a href="auth/logout" >Logout</a>';	
		}
		else
		{
			redirect('/login');
		}
		
		
	}

	private function check_login()
	{
		$sess_data = $_SESSION['login'];
		$user = $this->db->where('id', $sess_data['id'])->get('users')->result_array();
		// print_r($user[0]['login_status']);
		if($user[0]['login_status'] == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
