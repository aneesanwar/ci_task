<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	function __construct()
    {       
		parent::__construct();
		$this->load->model('post_model');
    }

	public function login()
	{
		$data = array();
		if($_POST)
		{
			$captcha = '';
	        if(isset($_POST['g-recaptcha-response']))
	        	$captcha = $_POST['g-recaptcha-response'];
			
			$resp	 = $this->post_model->recaptha($captcha); 
			if($resp['status'])
			{
				$this->form_validation->set_rules('username', 'Username', 'required');
				$this->form_validation->set_rules('pass', 'Password ', 'required');

				if ($this->form_validation->run() == FALSE)
				{
					$data['error'] = "Please fill all field";
				}
				else
				{

					$username = $this->input->post('username');
					$pass = $this->input->post('pass');
					$pass = md5($pass);
					$sql = "SELECT * FROM `users` WHERE email = '".$username."' OR uname = '".$username."' AND pass = '".$pass."'";
					$res= $this->db->query($sql)->result_array();
					if($res)
					{
						$_SESSION['login'] = $res[0];
						$this->db->where('id', $_SESSION['login']['id'])->update('users', array('login_status' => 1));
						$this->save_log(1, $_SESSION['login']['id']);
						redirect('/home');
					}
					else
					{
						$data['error'] = "Password not matched!";
					}
					
				}
				
			}
			else
			{
				$data['error'] = $resp['msg'];
			}
		}
		$this->load->view('login', $data);
	}

	public function signup()
	{
		$data = array();
		if($_POST)
		{
			$captcha= '';
        if(isset($_POST['g-recaptcha-response']))
          $captcha=$_POST['g-recaptcha-response'];
		
		  $resp	 = $this->post_model->recaptha($captcha); 
		// var_dump($resp);
		if($resp['status'])
		{
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('cpass', 'Password Confirmation', 'required');
			$this->form_validation->set_rules('pass', 'Password ', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$data['error'] = "Please fill all field";
				// $this->load->view('myform');
			}
			else
			{
				$name = $this->input->post('name');
				$username = $this->input->post('username');
				$pass = $this->input->post('pass');
				$cpass = $this->input->post('cpass');
				$email = $this->input->post('email');
				if($pass == $cpass)
				{
					// echo $pass;
					$in = array(
						"id" => '',
						"name" => $name,
						"email" => $email,
						"uname" => $username,
						"pass" => md5($pass),
					);
					$this->db->insert('users', $in);
					$data['success'] ="Sign up uccessfully. Pleae Login!";
				}
				else
				{
					$data['error'] = "Password not matched!";
				}
			}
		}
		else{
			$data['error'] = $resp['msg'];
		}	
		}
		
		$this->load->view('signup', $data);
	}

	public function logout()
	{
		if(isset($_SESSION['login']) && $_SESSION['login'] != '')
		{
			$this->save_log(2, $_SESSION['login']['id']);
			$this->db->where('id', $_SESSION['login']['id'])->update('users', array('login_status' => 0));
		}
		redirect('/login');
	}	

	private function save_log($type, $user_id)
	{
		$data = array(
			'user_id' => $user_id,
			'type' => $type
		);
		$this->db->insert('user_log', $data);
		return true;
	}	
}
