<?php
// class My_model extends yidas\Model
class Post_model extends CI_Model
{
    function __construct()
    {       
        parent::__construct();
    }

    function recaptha($captcha)
    {
        $res = array();
        if(!$captcha)
        {
            $res = array(
                "status"=>0,
                "msg"=> "Please fill recapcha"
            );
        }
        else
        {
            $pub_key = '6Lc6-JQUAAAAAPB42q7aNJneoMKFUPdt0ZPVb_7L';
            
            $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$pub_key&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
            if($response ['success'] == false)
            {
                $res = array(
                "status"=>0,
                "msg"=> "You are spammer ! Get the @$%K out</h2>"
            );

            }
            else
            {
                $res= array(
                    "status"=>1,
                    "data"=> $response
                );
            }
        }
        return $res;
    }
}		
?>